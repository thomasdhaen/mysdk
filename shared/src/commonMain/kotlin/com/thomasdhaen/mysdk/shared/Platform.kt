package com.thomasdhaen.mysdk.shared

expect class Platform() {
    val platform: String
}